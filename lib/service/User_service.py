from ..repository.User_repository import make_user_repository
from ..objects.User import make_user

class User_service():

    def __init__(self):
        self.user_repository = make_user_repository()

    def get_all_users(self):
        users = self.user_repository.get_all_users()
        print(users)
        return users

    def add_user(self, user):
        self.user_repository.add_user(user)


def make_user_service():
    return User_service()