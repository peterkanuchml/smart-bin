from sqlalchemy import Column, Numeric, ForeignKey
from base import Base


class Bin(Base):
    __tablename__ = 'bins'
    id = Column(Numeric, primary_key=True)
    user_id = Column(Numeric, ForeignKey('users.id'))
    longitude = Column('longitude', Numeric)
    latitude = Column('latitude', Numeric)
    altitude = Column('altitude', Numeric)

    def __init__(self, user_id, longitude, latitude, altitude):
        self.user_id = user_id
        self.longitude = longitude
        self.latitude = latitude
        self.altitude = altitude
