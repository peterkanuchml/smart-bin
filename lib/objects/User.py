from sqlalchemy import Column, Numeric, String, Integer
from base import Base
from marshmallow import Schema, fields

class User(Base):
    __tablename__ = 'users'
    
    
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    name = Column('name', String(200) )
    email = Column('email', String(200))
    password = Column('password', String(200))

    def __init__(self, name, email, password):
        self.name = name
        self.email = email
        self.password = password


class User_schema(Schema):
    id = fields.Number()
    name = fields.Str()
    email = fields.Str()
    password = fields.Str()


def make_user(name, email, password):
    return User(name, email, password)