from ..objects.User import User, User_schema
from flask import jsonify

from base import Session

class User_repository():

    def __init__(self):
        self.session = Session()

    def add_user(self, user_json):
        posted_user = User_schema(only=('name', 'email', 'password')).load(user_json)
        user = User(**posted_user.data)
        self.session.add(user)
        self.session.commit()

    def get_all_users(self):
        objects = self.session.query(User).all()
        user_schema = User_schema(many=True)
        return user_schema.dump(objects).data


def make_user_repository():
    return User_repository()