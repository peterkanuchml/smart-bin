from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from lib.service.User_service import make_user_service

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:postgres@localhost/postgres'
db = SQLAlchemy(app)

user_service = make_user_service()

@app.route("/")
def hello_world():
    return "Hello World!"

@app.route("/users")
def get_users():
    users = user_service.get_all_users()
    print(users)
    return jsonify(users),200

@app.route("/adduser", methods=['POST'])
def add_user():
    user = request.get_json()
    user_service.add_user(user)
    return "",201

if __name__ == "__main__":
    app.run(debug=True)

